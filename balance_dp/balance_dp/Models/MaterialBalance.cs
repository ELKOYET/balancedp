﻿namespace balance_dp.Models
{
    public class MaterialBalance
    {
        public float list5_C11 { get; set; }
        public float list5_C12 { get; set; }
        public float list5_C13 { get; set; }
        public float list5_C14 { get; set; }
        public float list5_C15 { get; set; }
        public float list5_C16 { get; set; }
        public float list5_C17 { get; set; }
        public float list5_C18 { get; set; }
        public float list5_C19 { get; set; }

        public string list5_C23 { get; set; }
        public string list5_C23_percent { get; set; }
        public string list5_C24 { get; set; }
        public string list5_C24_percent { get; set; }
        public string list5_C25 { get; set; }
        public string list5_C25_percent { get; set; }
        public string list5_C26 { get; set; }
        public string list5_C26_percent { get; set; }
        public string list5_C27 { get; set; }
        public string list5_C27_percent { get; set; }
        public string list5_C29 { get; set; }
        public string list5_C29_percent { get; set; }
        public string list5_C31 { get; set; }
        public string list5_C31_percent { get; set; }

        public float list5_InputMeterial { get; set; }
        public float list5_InputMaterial_percent{ get; set; }
        public string C33 { get; set; }
        public string C33_percent { get; set; }

        public string list5_C37 { get; set; }
        public string list5_C37_percent { get; set; }
        public string list5_C39 { get; set; }
        public string list5_C39_percent { get; set; }
        public string list5_C41 { get; set; }
        public string list5_C41_percent { get; set; }
        public string list5_C43 { get; set; }
        public string list5_C43_percent { get; set; }
        public string list5_C45 { get; set; }
        public string list5_C45_percent { get; set; }

        public float list5_Output { get; set; }
        public float list5_Output_percent { get; set; }

        public string C47 { get; set; }
        public string C47_persent { get; set; }

        public string list5_C50 { get; set; }
        public string list5_C50_percent { get; set; }

    }
    
    
}
